#include "GF.h"
#include <stdio.h>
#include <stdlib.h>

#define PRIME_POLINOM 0xB // 1011 x3+x+1
#define PRIME_ELEMENT 0x2 // 0010 x

static uint16_t N;
static uint16_t *field;
static uint16_t *inv_field;
static uint16_t deg;
static uint16_t prime_polinom;
static uint16_t prime_element;
static uint64_t add_count;
static uint64_t mult_count;

void print_field() {
    uint16_t i = 0;
    for(i = 0; i < N-1; i++) {
        printf("%d ", field[i]);
    }
    printf("\n");
    for(i = 0; i < N; i++) {
        printf("%d ", inv_field[i]);
    }
    printf("\n");
}

void reset_statistic() {
    add_count = 0;
    mult_count = 0;
}
void print_statistic() {
    printf("Additions: %lu \n"
           "Multiplications: %lu \n", add_count, mult_count);
}

uint16_t add (uint16_t b1, uint16_t b2) {
    add_count++;
    return b1 ^ b2;
}
uint16_t mult(uint16_t b1, uint16_t b2) {
    mult_count++;
    if (b1 == 0 || b2 == 0) {
        return 0;
    }
    return field[(inv_field[b1] + inv_field[b2]) % (N-1)];
}

uint16_t mult_prime(uint16_t b1, uint16_t pow) {
    mult_count++;
    if (b1 == 0) {
        return 0;
    }
    return field[(inv_field[b1] + (pow % (N-1)) ) % (N-1)];
}

static uint16_t _mult(uint16_t polinom1, uint16_t polinom2) {
    uint32_t pol_1 = polinom1;
    uint32_t result = 0;

    uint8_t i = 0;
    for (i = 0; i < sizeof(polinom1)*8; i++) {
        result = (polinom2 & 1<<i) ? result^(pol_1<<i) : result;
        if (result & deg) {
            result ^= prime_polinom;
        }
    }
    return (uint16_t) result;
}

uint16_t inv(uint16_t b) {
    if (b == 0) {
        printf("Error");
    }
    return field[ N-1 - inv_field[b] ];
}

void compute_field(uint16_t prime_pol, uint16_t prime_el, uint16_t Q) {

    N = Q;
    field = (uint16_t*) malloc( sizeof(uint16_t) * (N-1) );
    inv_field = (uint16_t*) malloc( sizeof(uint16_t) * N );
    prime_polinom = prime_pol;
    prime_element = prime_el;

    uint8_t i = 0;
    for (i = sizeof(prime_polinom)*8 - 1; i >= 0 ; i--) {
        if (prime_polinom & (1 << i)) {
            break;
        }
    }
    deg = 1 << i;
    if (deg != N) {
        printf("The number of elements is not suit to the degree of the prime polinom");
    }
    
    field[0] = 1;    
    for (i = 1; i < N-1; i++) {
        field[i] = _mult(field[i-1], prime_element);
    }

    for (i = 0; i < N-1; i++) {
        inv_field[field[i]] = i;
    }
}

uint16_t get_element(uint16_t pow) {
    return field[pow];
}

uint16_t get_pow(uint16_t element) {
    return inv_field[element];
}
void clear() {
    free(field);
    free(inv_field);
}

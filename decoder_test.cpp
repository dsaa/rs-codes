#include "DFT.h"
#include <stdio.h>
#include "coder.h"
#include "decoder.h"

#define Q 8               // a number of elements
#define PRIME_POLINOM 0xB // 1011 x3+x+1
#define PRIME_ELEMENT 0x2 // 0010 x

int main(void) {

    uint16_t N = Q;
    uint16_t t = 2;
    compute_field(PRIME_POLINOM, PRIME_ELEMENT, Q);
    printf("field\n");
    print_field();

    printf("\n");
    uint16_t L = 3;
    uint16_t input[N-1] = {1, 2, 3, 0, 0, 0, 0};
    uint16_t output[N-1] = {0};

    uint16_t i = 0;    

    printf("generic polinom\n");
    uint16_t generic_polinom[2*t+1] = {0};
    compute_generic_polinom( generic_polinom, t );
    for (i = 0; i <= 2*t; i++) {
        printf("%d ", generic_polinom[i]);
    }
    printf("\n");

    printf("code word\n");
    uint16_t code_word[N-1] = {0};
    //encode_gorner(output, input, 3, Q);
    encode_shift_register(code_word, input, generic_polinom, 2*t, 3);
    for (i = 0; i < N-1; i++) {
        printf("%d ", code_word[i]);
    }
    printf("\n");

    printf("code word zeros\n");
    uint16_t pol_zeros[N-1] = {0};
    chen_procedure(pol_zeros, code_word, N-2, Q);
    for (i = 0; i < N-3; i++) {
        printf("%d ", pol_zeros[i]);
    }
    printf("\n");

    printf("syndrome\n");
    code_word[2] = 5;
	code_word[4] = 5;
    uint16_t syndrome[2*t] = {0};
    compute_syndrome(syndrome, code_word, t, Q);
    for (i = 0; i < 2*t; i++) {
        printf("%d ", syndrome[i]);
    }
    printf("\n");

    printf("idft\n");
    uint16_t idft[N-1] = {0};
    invDFT(idft, code_word, Q);
    for (i = 0; i < N-1; i++) {
        printf("%d ", idft[i]);
    }
    printf("\n");    

    printf("errors locator\n");
    uint16_t locator[2*t] = {0};
    massey_decoder(locator, syndrome, t);
    for (i = 0; i < 2*t; i++) {
        printf("%d ", locator[i]);
    }
    printf("\n");

    printf("locator zeros\n");
    uint16_t zeros[2*t] = {0};
    uint16_t k = chen_procedure(zeros, locator, t, Q);
    for (i = 0; i < 2*t; i++) {
        printf("%d ", zeros[i]);
    }
    printf("\n");
    printf("%d \n", k);

    printf("derivative\n");
    uint16_t deriv[2*t] = {0};
    compute_derivative(deriv, locator, 7);
    for (i = 0; i < 2*t; i++) {
        printf("%d ", deriv[i]);
    }
    printf("\n");

    printf("error polinom\n");
    uint16_t error_polinom[2*t] = {0};
    compute_error_polinom(error_polinom, syndrome, locator, t);
    for (i = 0; i < 2*t; i++) {
        printf("%d ", error_polinom[i]);
    }
    printf("\n");

    clear();

    return 0;

}

CC = g++
CFLAGS = -g

all: clean dft_test coder_test decoder_test

dft_test: DFT.o GF.o
	$(CC) $(CFLAGS) GF.o DFT.o dft_test.cpp -o dft_test
coder_test: GF.o coder.o decoder.o
	$(CC) $(CFLAGS) GF.o decoder.o coder.o coder_test.cpp -o coder_test
decoder_test: GF.o decoder.o DFT.o coder.o
	$(CC) $(CFLAGS) GF.o DFT.o coder.o decoder.o decoder_test.cpp -o decoder_test

DFT.o: GF.o
	$(CC) $(CFLAGS) -c GF.o DFT.cpp -o DFT.o

coder.o: GF.o
	$(CC) $(CFLAGS) -c GF.o coder.cpp -o coder.o
decoder.o: GF.o
	$(CC) $(CFLAGS) -c GF.o decoder.cpp -o decoder.o
GF.o:
	$(CC) $(CFLAGS) -c GF.cpp -o GF.o
clean:
	rm -rf *_test *.o

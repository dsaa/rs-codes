#include "DFT.h"
#include <stdio.h>
#include "coder.h"
#include "decoder.h"

#define Q 8               // a number of elements
#define PRIME_POLINOM 0xB // 1011 x3+x+1
#define PRIME_ELEMENT 0x2 // 0010 x

int main(void) {

    uint32_t N = Q;
    uint16_t t = 2;
    compute_field(PRIME_POLINOM, PRIME_ELEMENT, Q);
    print_field();

    printf("\n");

    uint16_t input[N-1] = {1, 2, 3, 0, 0, 0, 0};
    uint16_t output[N-1] = {0};
    uint16_t input_1[N-1] = {0};


    for (uint32_t i = 0; i < N-1; i++) {
        printf("%d ", input[i]);
    }
    printf("\n");

    reset_statistic();
    DFT(output, input, Q);
    //invDFT(predicted, output, Q);
    for (int i = 0; i < N-1; i++) {
        printf("%d ", output[i]);
    }
    printf("\n");
    print_statistic();

    reset_statistic();
    invDFT(input_1, output, Q);
    for (int i = 0; i < N-1; i++) {
        printf("%d ", input_1[i]);
    }
    printf("\n");
    print_statistic();

    
    clear();

    return 0;

}

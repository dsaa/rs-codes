#ifndef CODER_H
#define CODER_H

#include <stdint.h>

void encode_gorner(uint16_t *output_word, uint16_t *input_message, uint16_t k,  uint16_t Q);

void compute_generic_polinom( uint16_t *output_word, uint16_t t );

void encode_shift_register(uint16_t *output_word,     uint16_t *input_message,
                           uint16_t *generic_polinom, uint16_t gen_deg, uint16_t msg_deg);

#endif

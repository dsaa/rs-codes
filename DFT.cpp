#include "DFT.h"
#include <stdlib.h>

uint16_t _binary_inverse_addr(uint16_t addr, uint16_t deg){
    uint16_t inverse_addr = 0;
    uint16_t i = 0;
    for (i = 0; i < deg; i++) {
        inverse_addr |= ((1 << i) & addr ? (1 << (deg-1 - i)) : 0);
    }
    return inverse_addr;
}

void reverse_cpy(uint16_t* dst_vector, const uint16_t* src_vector, uint16_t N){
    uint16_t i = 0;
    for (i = 0; i < N; i++) {
        dst_vector[_binary_inverse_addr(i, _log2(N))] = src_vector[i];
    }
}

uint8_t _log2(uint32_t a) {
    int16_t i = 0;
    for (i = sizeof(a)*8-1; i >= 0 ; i--) {
        if (a & (1<<i)) {
            return i;
        }
    }
}

void _butterfly(uint16_t *v, uint16_t N, uint16_t N_global) {
    uint16_t m = 0;
    uint16_t *result = NULL;
    if ((result = (uint16_t*)malloc(sizeof(uint16_t)*N)) == NULL ) {
        return;
    }

    if (N == 2) {
        result[0] = add(v[0], v[1]);
        result[1] = add(v[0], mult_prime(v[1], N/2) );
    } else { 
        for (m = 0; m < N/2; m++) {
            result[m      ] = add(v[m], mult_prime(v[m+N/2], m*N_global/N) ); 
            result[m + N/2] = add(v[m], mult_prime(v[m+N/2], m*N_global/N + N_global/2) );
        }
    }
    memcpy(v, result, N);
    free(result);
}


void DFT(uint16_t *output, const uint16_t *input, uint16_t Q) {

    uint16_t m = 0, n = 0;
    for (m = 0; m < Q-1; m++) {
        for (n = 0; n < Q-1; n++) {
            output[m] = add(output[m], mult_prime(input[n], m*n % (Q-1)) );
        }
    }
}

void invDFT(uint16_t *output, const uint16_t *input, uint16_t Q) {

    uint16_t m = 0, n = 0;
    for (m = 0; m < Q-1; m++) {
        for (n = 0; n < Q-1; n++) {
            output[m] = add(output[m], mult_prime(input[n], Q-1 - m*n % (Q-1)) );
        }
    }
}




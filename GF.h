
#include <stdint.h>

void print_field();

uint16_t get_element(uint16_t pow);
uint16_t get_pow(uint16_t element);

// compute the elements of GF(Q)
void compute_field(uint16_t prime_pol, uint16_t prime_el, uint16_t Q);

// elements multiplication
uint16_t mult (uint16_t element1, uint16_t element2);

// multiplication of element and prime element
uint16_t mult_prime (uint16_t element1, uint16_t pow);

// elements addition
uint16_t add    (uint16_t element1, uint16_t element2);

// return the inverse to the element b
uint16_t inv(uint16_t b);

void reset_statistic();
void print_statistic();

void clear();


#include "GF.h"
#include <memory.h>


uint16_t _binary_inverse_addr(uint16_t addr, uint16_t deg);

void reverse_cpy(uint16_t* dst_vector, const uint16_t* src_vector, uint16_t N);

void DFT(uint16_t *output, const uint16_t *input, uint16_t N);

void invDFT(uint16_t *output, const uint16_t *input, uint16_t N);

uint8_t _log2(uint32_t a);

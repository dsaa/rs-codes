#include "coder.h"
#include "GF.h"

void encode_gorner(uint16_t *output_word, uint16_t *input_message, uint16_t k,  uint16_t Q) {
    uint16_t i = 0;
    uint16_t j = 0;
    for (i = 0; i < Q-1; i++) {
        output_word[i] = input_message[k-1];
        for (j = k-1; j >= 1; j--)  {
            output_word[i] = add(mult_prime(output_word[i], i), input_message[j-1]);
        }
    }
}

void compute_generic_polinom( uint16_t *output_word, uint16_t t ) {
    uint16_t i = 0, j = 0;
    uint16_t gen_deg = 0;
    uint16_t tmp_d = 0, tmp_dd = 0;
    uint16_t generic_polinom[2*t+1] = {0};
    output_word[0] = get_element(1);
    output_word[1] = 1;
    for (gen_deg = 2; gen_deg <= 2*t; gen_deg++) {
        tmp_dd = 0;        
        tmp_d = output_word[0];
        output_word[0] = mult_prime(output_word[0], gen_deg);
        for (i = 1; i <= gen_deg; i++) {
            tmp_dd = tmp_d;
            tmp_d = output_word[i];
            output_word[i] = add(tmp_dd, mult_prime(output_word[i], gen_deg) );
            
        }
    }
}

void encode_shift_register(uint16_t *output_word,     uint16_t *input_message,
                           uint16_t *generic_polinom, uint16_t gen_deg, uint16_t msg_deg) {
    uint16_t i = 0, j = 0;
    for (i = 0; i <= gen_deg; i++) {
        for(j = 0; j <= msg_deg; j++) {
            output_word[i + j] = add( output_word[i + j], mult(generic_polinom[i], input_message[j]) );
        }
    }
}


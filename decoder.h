#ifndef DECODER_H
#define DECODER_H

#include <stdint.h>

void compute_syndrome(uint16_t *output_syndrome, const uint16_t *input_word, uint16_t t, uint16_t Q);

void massey_decoder(uint16_t *A, const uint16_t *S, uint16_t t);

uint16_t chen_procedure(uint16_t *output_zeros, const uint16_t *input_polinom, uint16_t t, uint16_t Q);

void compute_derivative(uint16_t *derivative, uint16_t *src, uint16_t len);

void compute_error_polinom(uint16_t *error_polinom,   uint16_t *syndrome_polinom,
                           uint16_t *locator_polinom, uint16_t t);

#endif

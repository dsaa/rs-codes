#include "DFT.h"
#include <stdio.h>
#include "coder.h"
#include "decoder.h"

#define Q 8               // a number of elements
#define PRIME_POLINOM 0xB // 1011 x3+x+1
#define PRIME_ELEMENT 0x2 // 0010 x

int main(void) {

    uint32_t N = Q;
    uint16_t t = 2;
    compute_field(PRIME_POLINOM, PRIME_ELEMENT, Q);
    print_field();

    printf("\n");

    uint16_t input[N-1] = {1, 2, 3, 0, 0, 0, 0};
    uint16_t output[N-1] = {0};
    
    printf("generic polinom\n");
    uint16_t generic_polinom[5] = {0};
    compute_generic_polinom( generic_polinom, t );
    for (int i = 0; i <= 2*t; i++) {
        printf("%d ", generic_polinom[i]);
    }
    printf("\n");

    printf("code word\n");
    //encode_gorner(output, input, 3, Q);
    encode_shift_register(output, input, generic_polinom, 4, 3);
    for (int i = 0; i < N-1; i++) {
        printf("%d ", output[i]);
        //output[i] = 0;
    }
    printf("\n");

    printf("word zeros\n");
    uint16_t pol_zeros[4] = {0};
    chen_procedure(pol_zeros, generic_polinom, 2*t, Q);
    for (int i = 0; i < 2*t; i++) {
        printf("%d ", pol_zeros[i]);
    }
    printf("\n");

    clear();

    return 0;

}

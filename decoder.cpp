#include "decoder.h"
#include "GF.h"

static void shift_left(uint16_t *shifted, const uint16_t* src, uint16_t len) {
    uint16_t i = 0;
    shifted[0] = 0;
    for (i = 1; i < len; i++) {
        shifted[i] = src[i-1];
    }
}

void compute_syndrome(uint16_t *output_syndrome, const uint16_t *input_word, uint16_t t, uint16_t Q) {
    uint16_t i = 0, j = 0;
    for (i = 0; i < 2*t; i++) {
        output_syndrome[i] = input_word[Q-2];
        for (j = Q-2; j >= 1; j--)  {
            output_syndrome[i] = add(mult_prime(output_syndrome[i], i+1), input_word[j-1]);
            
        }
    }
}

uint16_t chen_procedure(uint16_t *output_zeros, const uint16_t *input_polinom, uint16_t t, uint16_t Q) {
    uint16_t i = 0;
    uint16_t j = 0;
    uint16_t result = 0;
    uint16_t k = 0;

    for (i = 0; i <= Q-2; i++) {
        result = input_polinom[t];
        for (j = t; j >= 1; j--)  {
            result = add(mult_prime(result, i), input_polinom[j-1]);
        }
        if (result == 0) {
            if (k > t) {
                break;
            }
            output_zeros[k] = i;
            k++;
        }
    }
    return k;
}

void massey_decoder(uint16_t *A, const uint16_t *S, uint16_t t) {
    uint16_t D = 0;
    uint16_t d = 0;
    uint16_t L = 0;
    //uint16_t A[2*t] = {0};
    uint16_t B[2*t] = {0};
    uint16_t tmpA[2*t] = {0};
    uint16_t tmpB[2*t] = {0};

    A[0] = 1;
    B[0] = 1;
    
    uint16_t r = 0;
    for (r = 1; r <= 2*t; r++) {
        D = 0;
        uint16_t j = 0;
        for (j = 0; j <= r-1; j++) {
            D = add(D, mult( A[j], S[r-j -1] ) ); // S1,S2,S3,...,S2t => -1 (to do enumeration since 1)
        }

        if (D != 0) {

            shift_left( tmpB, B, 2*t);

            for (j = 0; j <= r; j++) {
                tmpA[j] = add( A[j], mult(D, tmpB[j]) );
            }
            if (2*L <= r-1) {
                for (j = 0; j <= r; j++) {
                    B[j] = mult(inv(D), A[j]);
                }
                L = r - L;
            } else {
                for (j = 0; j <= r; j++) {
                    B[j] = tmpB[j];
                }            
            }

            for (j = 0; j <= r; j++) {
                A[j] = tmpA[j];
            }            
        } else {
            shift_left(B, B, 2*t);
        }
    }
}

void compute_derivative(uint16_t *derivative, uint16_t *src, uint16_t len) {
    uint16_t i = 0;
    for (i = 1; i < len; i++) {
        derivative[i-1] = i%2 ? src[i] : 0 ;
    }
}

void compute_error_polinom(uint16_t *error_polinom,   uint16_t *syndrome_polinom,
                           uint16_t *locator_polinom, uint16_t t) {
    uint16_t i = 0, j = 0;
    for (i = 0; i <= t+1; i++) {
        for(j = 0; j <= 2*t; j++) {
            if (i + j < 2*t) {  // mod(x^2t)
                error_polinom[i + j] = add( error_polinom[i + j], mult(locator_polinom[i], syndrome_polinom[j]) );
            }
        }
    }
}

